import { Component, OnInit } from '@angular/core';
import { Frase } from '../shared/frase.model';
import { frases } from './frases.mock';


@Component({
	selector: 'app-painel',
	templateUrl: './painel.component.html',
	styleUrls: ['./painel.component.scss']
})
export class PainelComponent implements OnInit {

	public frases: Array<Frase> = frases;
	public instrucao = "Traduza a frase:";
	public resposta = '';
	public rodada = 0;
	public rodadaFrase: Frase;
	public progresso = 0;
	constructor() {
		console.log(frases);
		}

	ngOnInit() {
		this.atualizaRodada();
	}

	public atualizaResposta(resposta: Event): void {
		this.resposta = (<HTMLInputElement> resposta.target).value;
	}

	public verificarResposta(): void {
		console.log('verificar resposta:', this.resposta);
		if (this.resposta.trim().toLowerCase() === this.rodadaFrase.frasePtBr.trim().toLowerCase()) {
			this.progresso += (100 / this.frases.length);
			if (this.rodada < this.frases.length - 1) {
				this.rodada ++;
				this.atualizaRodada();
			}
			console.log('acertou', this.progresso);
		}
		else {
			alert('errou');
		}
	}

	public atualizaRodada (): void {
		this.rodadaFrase = frases[this.rodada];
		this.resposta = '';
	}
}
