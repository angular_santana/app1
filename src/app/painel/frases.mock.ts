import { Frase } from "../shared/frase.model";

export const frases: Array<Frase> = [
	{fraseEng: "I like potato", frasePtBr: "Eu gosto de batata"},
	{fraseEng: "I like to learn", frasePtBr: "Eu gosto de estudar"},
	{fraseEng: "I like cachaça", frasePtBr: "Eu gosto de cachaça"},
	{fraseEng: "I like torresmo", frasePtBr: "Eu gosto de torresmo"},
];
